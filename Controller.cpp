#include "Controller.h"


bool Comparator(const Files &left, const Files &right)
{
	return left.size < right.size;
}

void Controller::SelectFolder(QFileInfoList& lw, QFileDialog* dialog, QDir* dir)
{
	dialog->exec();
	dir->setPath(dialog->directory().path());

	lw.clear();
	lw = dir->entryInfoList();
}

std::vector<QString> Controller::Compare(QFileInfoList& fileList1, QFileInfoList& fileList2)
{
	std::vector<Files> files = MakeVectorOfFiles(fileList1, fileList2);
	std::vector<QString> sameSizedFiles;
	std::vector<QString> sameFiles;
	
	//Find files with same size
	for (int i = 1; i < files.size(); i++)
	{
		if ((files[i].size == files[i - 1].size) && (files[i].folder != files[i - 1].folder))
		{
			sameSizedFiles.push_back(files[i - 1].name);
			sameSizedFiles.push_back(files[i].name);
		}
	}

	//Compare adjacent files in sorted vector
	if (sameSizedFiles.size() > 0)
	{
		for (int i = 0; i < sameSizedFiles.size(); i += 2)
		{
			const int buferSize = 128;
			bool equal = true;
			QFile file1, file2;
			file1.setFileName(sameSizedFiles[i]);
			file2.setFileName(sameSizedFiles[i + 1]);
			file1.open(QIODevice::ReadOnly);
			file2.open(QIODevice::ReadOnly);

			std::unique_ptr<char[]> bufer1(new char[buferSize]);
			std::unique_ptr<char[]> bufer2(new char[buferSize]);

			do
			{
				file1.read(bufer1.get(), buferSize);
				file2.read(bufer2.get(), buferSize);

				equal = (memcmp(bufer1.get(), bufer2.get(), buferSize) == 0);

				if (file1.atEnd() && equal)
				{
					sameFiles.push_back(file1.fileName());
					sameFiles.push_back(file2.fileName());
					equal = false;
				}
			} while (equal);
		}
	}
	return sameFiles;
}

std::vector<Files> Controller::MakeVectorOfFiles(QFileInfoList& fileList1, QFileInfoList& fileList2)
{
	std::vector<Files> files(fileList1.count() + fileList2.count());

	//THIS CODE LOOKS SO AWFUL NEED TO REWORK
	int i = 0;
	for (i; i < fileList1.count(); i++)
	{
		files[i].folder = false;
		files[i].name = fileList1[i].absoluteFilePath();
		files[i].size = fileList1[i].size();
	}
	int j = 0;

	for (i, j; i < files.size(); i++, j++)
	{
		files[i].folder = true;
		files[i].name = fileList2[j].absoluteFilePath();
		files[i].size = fileList2[j].size();
	}
	//AND HERE THIS SHIT ENDS

	//Pointer on custom comparator, yes. See std::sort on cppreference
	std::sort(files.begin(), files.end(), &Comparator);

	return files;
}