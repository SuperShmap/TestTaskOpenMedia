#pragma once
#include "qheaders.h"
#include <memory>

struct Files
{
	bool folder; //Don't want to make special enum for this. Bool will be enough
	QString name;
	int size;
};

static class ModelController
{
public:
	static void SelectFolder(QFileInfoList& lw, QFileDialog* dialog, QDir* dir);
	static void Compare(QFileInfoList& fileList1, QFileInfoList& fileList2);
	static std::vector<Files> MakeVectorOfFiles(QFileInfoList& fileList1, QFileInfoList& fileList2);
};