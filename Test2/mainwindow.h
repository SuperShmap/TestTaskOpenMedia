#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qheaders.h"
#include "ModelController.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

	QPushButton* pb_1stFolder;
	QPushButton* pb_2ndFolder;
	QPushButton* pb_compare;

	QLabel* lb_title;

	QGroupBox* gb_1stFolder;
	QGroupBox* gb_2ndFolder;

	QGridLayout* grid;

	QFileDialog* fd_dialog;

	QDir* dir_1stFolder;
	QDir* dir_2ndFolder;
	QFileInfoList fi_1stFolder;
	QFileInfoList fi_2ndFolder;
	QListWidget* lw_1stFolder;
	QListWidget* lw_2ndFolder;

public slots:
	void SelectFirstFolder();
	void SelectSecondFolder();
	void Compare();

private:
	
};

#endif // MAINWINDOW_H
