#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
	fd_dialog = new QFileDialog(this);
	fd_dialog->setFileMode(QFileDialog::Directory);

	lb_title = new QLabel("Select folders and press COMPARE", this);

	pb_1stFolder = new QPushButton("Open", this);
	pb_2ndFolder = new QPushButton("Open", this);
	pb_compare = new QPushButton("COMPARE", this);

	lw_1stFolder = new QListWidget(this);
	lw_2ndFolder = new QListWidget(this);

	grid = new QGridLayout(this);
	grid->addWidget(lb_title, 0, 0, 1, 2, Qt::AlignCenter);
	grid->addWidget(lw_1stFolder, 1, 0);
	grid->addWidget(lw_2ndFolder, 1, 1);
	grid->addWidget(pb_1stFolder, 2, 0);
	grid->addWidget(pb_2ndFolder, 2, 1);
	grid->addWidget(pb_compare, 3, 0, 1, 2, Qt::AlignCenter);

	dir_1stFolder = new QDir;
	dir_1stFolder->setFilter(QDir::Files);
	dir_2ndFolder = new QDir;
	dir_2ndFolder->setFilter(QDir::Files);

	QWidget* central = new QWidget(this);
	central->setLayout(grid);
	this->setCentralWidget(central);

	connect(pb_1stFolder, SIGNAL(clicked()), this, SLOT(SelectFirstFolder()));
	connect(pb_2ndFolder, SIGNAL(clicked()), this, SLOT(SelectSecondFolder()));
	connect(pb_compare, SIGNAL(clicked()), this, SLOT(Compare()));
}

MainWindow::~MainWindow()
{
	if (dir_1stFolder != nullptr)
		delete dir_1stFolder;
	if (dir_2ndFolder != nullptr)
		delete dir_2ndFolder;
}

void MainWindow::SelectFirstFolder()
{
	Controller::SelectFolder(fi_1stFolder, fd_dialog, dir_1stFolder);
	lw_1stFolder->clear();
	lw_1stFolder->addItems(dir_1stFolder->entryList());
}

void MainWindow::SelectSecondFolder()
{
	Controller::SelectFolder(fi_2ndFolder, fd_dialog, dir_2ndFolder);
	lw_2ndFolder->clear();
	lw_2ndFolder->addItems(dir_2ndFolder->entryList());
}

void MainWindow::Compare()
{
	std::vector<QString> sameFiles = Controller::Compare(fi_1stFolder, fi_2ndFolder);
	if (sameFiles.size() == 0)
	{
		QMessageBox mb;
		mb.setInformativeText("No equal files");
	}
	else
	{
		ResultDialog result(sameFiles);
		result.exec();
	}
}