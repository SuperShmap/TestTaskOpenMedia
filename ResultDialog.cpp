#include "ResultDialog.h"

ResultDialog::ResultDialog(std::vector<QString> files)
{
	this->setWindowTitle("SameFiles");
	QVBoxLayout* layout = new QVBoxLayout(this);

	for (int i = 0; i < files.size(); i += 2)
	{
		QListWidget* lw = new QListWidget(this);
		lw->addItem(files[i]);
		lw->addItem(files[i + 1]);
		layout->addWidget(lw);
	}
}

ResultDialog::ResultDialog()
{
}

ResultDialog::~ResultDialog()
{
}