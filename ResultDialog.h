#pragma once

#include <qdialog.h>
#include <qlistwidget.h>
#include <qvboxlayout>

class ResultDialog :
	public QDialog
{
	Q_OBJECT

public:
	ResultDialog(std::vector<QString> files);

	ResultDialog();
	~ResultDialog();
};